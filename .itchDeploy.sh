#!/bin/bash

#install butler
wget -O butler.zip https://broth.itch.ovh/butler/linux-amd64/LATEST/archive/default
unzip butler.zip
chmod +x butler
./butler -V


ls -R

user=greenf0x
game=the-game-is-a-lier

pushBuild()
{
    ./butler push "$1" "$user/$game:$2"
}
pushBuild "$(ls public/Asteroid_Miner_LinuxX11_*)" Dev_Linux
pushBuild "$(ls public/Asteroid_Miner_Windows*)" Dev_Windows


mv public/HTML5/*.html "public/HTML5/index.html" 

pushBuild "public/HTML5" Dev_HTML5


